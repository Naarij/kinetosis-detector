from flask import render_template, flash, redirect, url_for, request, current_app, jsonify
from flask_login import current_user, login_required
from app import db
from app.main import bp
from app.main.forms import ThresholdForm, InteractionForm
from app.models import InteractionShortcut, Session, SessionInteractionShortcut, SessionDevice
from app.devices import *


def get_session_info():
    active_session_id = current_user.get_active_session_id()
    if active_session_id:
        return '<span>Sitzung aktiv <a class="btn btn-primary btn-sm ml-2" href="{}">Sitzung beenden</a>'\
            .format(url_for('main.stop_new_session'))
    else:
        return 'Keine Sitzung aktiv <a class="btn btn-primary btn-sm ml-2" href="{}">Neue Sitzung</a>'\
            .format(url_for('main.select_interactions'))


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    return render_template('index.html', title='Home', heading='Hallo ' + current_user.name,
                           session_info=get_session_info())


@bp.route('/threshold', methods=['GET', 'POST'])
@login_required
def threshold():
    form = ThresholdForm(threshold=current_user.threshold)
    if form.validate_on_submit():
        user = current_user
        user.threshold = form.threshold.data
        db.session.add(user)
        db.session.commit()
        flash('Neuer Schwellenwert gespeichert.')
    return render_template('threshold.html', title='Home', heading='Einstellen des Kinetose-Schwellenwerts',
                           session_info=get_session_info(), form=form)


@bp.route('/shortcuts', methods=['GET', 'POST'])
@login_required
def shortcuts():
    form = InteractionForm()
    interaction_shortcuts = InteractionShortcut.query.filter_by(id_user=current_user.id)
    if form.validate_on_submit():
        interaction_shortcut = InteractionShortcut(id_user=current_user.id, shortcut=form.shortcut.data)
        db.session.add(interaction_shortcut)
        db.session.commit()
        flash('Neuer Shortcut gespeichert.')
        return redirect(url_for('main.shortcuts'))
    return render_template('interactions.html', title='Home', heading='Verwalten der Interaktions-Shortcuts',
                           session_info=get_session_info(), form=form, interaction_shortcuts=interaction_shortcuts)


@bp.route('/shortcuts/delete/<id>', methods=['GET', 'POST'])
@login_required
def shortcut_delete(id):
    interaction_shortcut = InteractionShortcut.query.filter_by(id=id, id_user=current_user.id).first_or_404()
    db.session.delete(interaction_shortcut)
    db.session.commit()
    flash('Shortcut gelöscht.')
    return redirect(url_for('main.shortcuts'))


@bp.route('/websession/start', methods=['GET', 'POST'])
@login_required
def start_new_session():
    devices = request.form.getlist('device_checkbox')
    for datamanager in deviceManager.keys():
        deviceManager[datamanager].reset()
    if len(devices) < 1:
        flash('Bitte mindestens ein Gerät wählen!')
        return redirect(url_for('main.select_devices'), code=307)
    interaction_ids = request.form.getlist('interaction_checkbox')
    if len(interaction_ids) < 1:
        flash('Bitte mindestens einen Interaktions-Shortcut wählen!')
        return redirect(url_for('main.select_interactions'))
    new_session = Session(id_user=current_user.id)
    db.session.add(new_session)
    db.session.commit()
    for device in devices:
        new_session_device = SessionDevice(id_session=current_user.get_active_session_id(), device=device)
        db.session.add(new_session_device)
        db.session.commit()
    for interaction_id in interaction_ids:
        new_session_interaction_shortcut = SessionInteractionShortcut(id_session=current_user.get_active_session_id(),
                                                                      id_interaction_shortcut=interaction_id)
        db.session.add(new_session_interaction_shortcut)
        db.session.commit()
    return redirect(url_for('main.index'))


@bp.route('/websession/stop', methods=['GET', 'POST'])
@login_required
def stop_new_session():
    active_session_id = current_user.get_active_session_id()
    if active_session_id:
        session = Session.query.filter_by(id=active_session_id, id_user=current_user.id).first_or_404()
        session.end()
        db.session.commit()
    return redirect(url_for('main.index'))


@bp.route('/websession/new/interactions', methods=['GET', 'POST'])
@login_required
def select_interactions():
    if current_user.get_active_session_id():
        flash('Es ist bereits eine Sitzung aktiv.')
        return redirect(url_for('main.index'))
    interaction_shortcuts = InteractionShortcut.query.filter_by(id_user=current_user.id)
    return render_template('interaction_selection.html', title='Home',
                           heading='Bitte die Interaktions-Shortcuts der Sitzung wählen', session_info='Neue Sitzung',
                           interaction_shortcuts=interaction_shortcuts)


@bp.route('/websession/new/devices', methods=['GET', 'POST'])
@login_required
def select_devices():
    interaction_ids = request.form.getlist('interaction_checkbox')
    if len(interaction_ids) < 1:
        flash('Bitte mindestens einen Interaktions-Shortcut wählen!')
        return redirect(url_for('main.select_interactions'))
    return render_template('device_selection.html', title='Home', devices=deviceClasses,
                           heading='Bitte die Geräte der Sitzung wählen', session_info='Neue Sitzung',
                           interactions=interaction_ids)


@bp.route('/websession/prvalue', methods=['GET'])
@login_required
def get_web_session_pr_value():
    users_active_session = current_user.get_active_session()
    pr = users_active_session.current_pr_value
    return render_template('blank.html', title='Home', content=pr)
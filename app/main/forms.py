from flask_wtf import FlaskForm
from wtforms import FloatField, SubmitField, StringField
from wtforms.validators import DataRequired


class ThresholdForm(FlaskForm):
    threshold = FloatField('Treshold', validators=[DataRequired()])
    submit = SubmitField('Speichern')


class InteractionForm(FlaskForm):
    shortcut = StringField('Shortcut', validators=[DataRequired()])
    submit = SubmitField('Speichern')

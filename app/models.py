from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from app import db, login
from hashlib import md5
import base64
import os
from datetime import datetime, timedelta



class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    name = db.Column(db.String(120), index=True)
    threshold = db.Column(db.Float)
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)

    interaction_shortcuts = db.relationship('InteractionShortcut', backref='interaction', lazy='dynamic')
    sessions = db.relationship('Session', backref='session', lazy='dynamic')

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.name.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(digest, size)

    def get_active_session_id(self):
        sessions = self.sessions.order_by(Session.id.desc())
        for session in sessions:
            if session.check_if_active():
                return session.id
        return None

    def get_active_session(self):
        session_id = self.get_active_session_id()
        return Session.query.filter_by(id=session_id).first_or_404()

    def get_token(self, expires_in=7200):
        now = datetime.utcnow()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = base64.b64encode(os.urandom(24)).decode('utf-8')
        self.token_expiration = now + timedelta(seconds=expires_in)
        db.session.add(self)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.utcnow() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.utcnow():
            return None
        return user


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class InteractionShortcut(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))
    shortcut = db.Column(db.String(120), index=True)

    def __repr__(self):
        return '<Shortcut {}>'.format(self.shortcut)


class Session(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    id_user = db.Column(db.Integer, db.ForeignKey('user.id'))
    start_datetime = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    stop_datetime = db.Column(db.DateTime, index=True)
    current_pr_value = db.Column(db.Float, default=0.0)

    def __repr__(self):
        return '<Session {}>'.format(str(self.start_datetime))

    def check_if_active(self):
        return self.stop_datetime is None

    def end(self):
        self.stop_datetime = datetime.utcnow()

    def get_interaction_shortcut_list(self):
        session_interaction_shortcuts = SessionInteractionShortcut.query.filter_by(id_session=self.id)
        session_interaction_shortcut_list = []
        for session_interaction_shortcut in session_interaction_shortcuts:
            shortcut = InteractionShortcut.query.filter_by(id=session_interaction_shortcut.id_interaction_shortcut,
                                                           id_user=self.id_user).first_or_404()
            session_interaction_shortcut_list.append(shortcut)
        return session_interaction_shortcut_list

    def get_device_list(self):
        pass

    def to_dict(self):
        session_interaction_shortcuts = SessionInteractionShortcut.query.filter_by(id_session=self.id)
        session_interaction_shortcut_list = []
        for session_interaction_shortcut in session_interaction_shortcuts:
            session_interaction_shortcut_list.append(InteractionShortcut.query.filter_by(
                id=session_interaction_shortcut.id_interaction_shortcut).first_or_404().shortcut)
        session_devices = SessionDevice.query.filter_by(id_session=self.id)
        session_device_list = []
        for session_device in session_devices:
            session_device_list.append(session_device.device)
        data = {
            'id': self.id,
            'id_user': self.id_user,
            'start_datetime': self.start_datetime,
            'current_pr_value': self.current_pr_value,
            'session_interaction_shortcut': session_interaction_shortcut_list,
            'session_devices': session_device_list,
            'threshold': User.query.filter_by(id=self.id_user).first_or_404().threshold
        }
        return data

    def add_to_pr(self, v: float):
        if v >= 1:
            v = 0.99
        ratio = (1-self.current_pr_value) * v
        self.current_pr_value = self.current_pr_value + ratio
        if self.current_pr_value < 0:
            self.current_pr_value = 0
        db.session.commit()


class SessionInteractionShortcut(db.Model):
    id_session = db.Column(db.Integer, db.ForeignKey('session.id'), primary_key=True)
    id_interaction_shortcut = db.Column(db.Integer, db.ForeignKey('interaction_shortcut.id'), primary_key=True)


class SessionDevice(db.Model):
    id_session = db.Column(db.Integer, db.ForeignKey('session.id'), primary_key=True)
    device = db.Column(db.String(120), primary_key=True)

import math

def calculateDistanceEuclid(vector1, vector2):
    return math.sqrt(math.pow(vector2[0] - vector1[0], 2) + math.pow(vector2[1] - vector1[1], 2) + math.pow(vector2[2] - vector1[2], 2))

def calculateDistanceManhatten(vector1, vector2):
    return abs(vector2[0] - vector1[0]) + abs(vector2[1] - vector1[1]) + abs(vector2[2] - vector1[2])

def calculateDistanceMaximum(vector1, vector2):
    return max(abs(vector2[0] - vector1[0]), abs(vector2[1] - vector2[1]), abs(vector2[2] - vector1[2]))

def get_zero_vector():
    return(0, 0, 0)
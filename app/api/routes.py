from flask import jsonify, g, abort, request
from app import db
from app.api import bp
from app.models import Session, SessionDevice
from app.api.auth import basic_auth
from app.api.auth import token_auth

from app.devices import *
from app.devices.eye_positional_tracking_device import EyeTrackingMode

from datetime import datetime
import dateutil.parser

@bp.route('/token', methods=['POST'])
@basic_auth.login_required
def get_token():
    token = g.current_user.get_token()
    db.session.commit()
    return jsonify({'token': token})


@bp.route('/token', methods=['DELETE'])
@token_auth.login_required
def revoke_token():
    g.current_user.revoke_token()
    db.session.commit()
    return '', 204


@bp.route('/session', methods=['GET'])
@token_auth.login_required
def get_session():
    users_active_session_id = g.current_user.get_active_session_id()
    if users_active_session_id is None:
        abort(404)
    return jsonify(Session.query.filter_by(id=users_active_session_id).first_or_404().to_dict())


@bp.route('/session/prvalue', methods=['GET'])
@token_auth.login_required
def get_session_pr_value():
    users_active_session_id = g.current_user.get_active_session_id()
    if users_active_session_id is None:
        abort(404)
    return jsonify({'current_pr_value': Session.query.filter_by(id=users_active_session_id).first_or_404()
                   .current_pr_value})


@bp.route('/session/device/<string:device>', methods=['GET'])
@token_auth.login_required
def get_session_device(device):
    users_active_session_id = g.current_user.get_active_session_id()
    if users_active_session_id is None:
        abort(404)
    device_entry = deviceClasses[device].query.filter_by(id_session=users_active_session_id)\
        .order_by(deviceClasses[device].timestamp.desc()).first_or_404()
    return jsonify(device_entry.to_dict())


@bp.route('/session/device/<string:device>', methods=['POST'])
@token_auth.login_required
def post_session_device(device):
    users_active_session_id = g.current_user.get_active_session_id()
    if users_active_session_id is None:
        abort(404)
    SessionDevice.query.filter_by(id_session=users_active_session_id, device=device).first_or_404()
    currentDeviceManager = deviceManager[device]
    new_device_data = deviceClasses[device]()
    new_device_data.id_session = users_active_session_id
    data = request.get_json() or {}
    for item in data:
        if item == 'timestamp':
            timestamp = dateutil.parser.parse(data[item])
            setattr(new_device_data, item, timestamp)
            continue
        setattr(new_device_data, item, data[item])
    new_device_data.calibration = False
    currentDeviceManager.data = new_device_data
    currentDeviceManager.start_tracking()
    db.session.add(new_device_data)
    db.session.commit()
    new_device_data.calculate_pr()
    return get_session_device(device)

#Bei manchen Trackern ist hier die Ausgabe buggy (Error 404 - Die Methode wird dennoch ausgeführt)
@bp.route('/session/device/<string:device>/settings', methods=['POST'])
@token_auth.login_required
def post_session_device_settings(device):
    users_active_session_id = g.current_user.get_active_session_id()
    if users_active_session_id is None:
        abort(404)
    currentDeviceManager = deviceManager[device]
    data = request.get_json() or {}
    for item in data:
        setattr(currentDeviceManager, item, data[item])
    return get_session_device(device)

@bp.route('/session/device/<string:device>/calibrate', methods=['POST'])
@token_auth.login_required
def post_session_device_calibrate(device):
    users_active_session_id = g.current_user.get_active_session_id()
    if users_active_session_id is None:
        abort(404)
    SessionDevice.query.filter_by(id_session=users_active_session_id, device=device).first_or_404()
    currentDeviceManager = deviceManager[device]
    new_device_data = deviceClasses[device]()
    new_device_data.id_session = users_active_session_id
    data = request.get_json() or {}
    for item in data:
        if item == 'timestamp':
            timestamp = dateutil.parser.parse(data[item])
            setattr(new_device_data, item, timestamp)
            continue
        setattr(new_device_data, item, data[item])
    new_device_data.calibration = True
    currentDeviceManager.data = new_device_data
    currentDeviceManager.start_calibrating()
    db.session.add(new_device_data)
    db.session.commit()
    return get_session_device(device)


@bp.route('/session/device/<string:device>/history/<int:count>', methods=['GET'])
@token_auth.login_required
def get_session_device_history(device, count):
    users_active_session_id = g.current_user.get_active_session_id()
    if users_active_session_id is None:
        abort(404)
    device_entries = deviceClasses[device].query.filter_by(id_session=users_active_session_id) \
        .order_by(deviceClasses[device].timestamp.desc()).limit(count)
    device_entry_list = []
    for device_entry in device_entries:
        device_entry_list.append(device_entry.to_dict())
    return jsonify({'history': device_entry_list})

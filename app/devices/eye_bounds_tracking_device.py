from app.devices import *
from app.devices.base_device import BaseDevice, BaseDeviceManager
from app.devices.eye_positional_tracking_device import EyeTrackingMode
from app import db
from enum import Enum
from sqlalchemy import func
from app.util import distance
import queue
import math

class EyeBoundsTrackingManager(BaseDeviceManager):

    ReferencePoint = (0, 0, 0)
    Mode = EyeTrackingMode.Euclid
    CompareValue = 3

    ConstantCompare = True

    def start_calibrating(self):
        if self.ConstantCompare:
            return
        self.add_to_compare(self.data.get_current_distance(self.Mode, self.ReferencePoint))

    def start_tracking(self):
        if self.CompareValue == 0:
            print("WARNING: Reference value is 0. Set to 0.0001 to ensure comparison. Resulting Pr may be too sensitive. Adjust via manipulating the calibration value or by sending more calibration data.")
            self.CompareValue = 0.0001
        self.data.prValue = self.determine_pr_value() * self.weight

    def add_to_compare(self, value: float):
        self.CalibrationValue = self.CalibrationValue + value
        self.AmountCalibrationValues = self.AmountCalibrationValues + 1
        self.CompareValue = self.CalibrationValue / self.AmountCalibrationValues

    def determine_percent_difference(self, value: float, ref: float):
        difference = value/ref 
        if difference > 1:
            return difference - 1
        elif difference < 1:
            return (1 - difference) * (-1)
        else:
            return 0

    def determine_pr_value(self):
        currentValue = self.data.get_current_distance(self.Mode, self.ReferencePoint)
        ps = self.determine_percent_difference(currentValue, self.CompareValue)
        differences = {}
        invert = 1
        if ps  < 0:
            invert = -1
        for element in self.PrValueTable.keys():
            differences[element] = (ps - float(element)) * invert
        sor = sorted(differences.items(), key=lambda x:x[1])
        pr = next(iter(sor))
        return float(self.PrValueTable[pr[0]]) 


@register_device_class
class EyeBoundsTracker(BaseDevice):
    x = db.Column(db.FLOAT, index=True)
    y = db.Column(db.FLOAT, index=True)
    z = db.Column(db.FLOAT, index=True)

    calibrationValue = 0
    prValue = 0

    def to_dict(self):
        data = super().to_dict()
        data['x'] = self.x
        data['y'] = self.y
        data['z'] = self.z
        data['calibration'] = self.calibration
        return data

    @staticmethod
    def get_device_manager():
        return EyeBoundsTrackingManager

    def get_current_vector(self):
        return [self.x, self.y, self.z]

    def get_current_distance(self, mode, referencePoint):
        if mode == EyeTrackingMode.Euclid:
            return distance.calculateDistanceEuclid(referencePoint, self.get_current_vector())
        elif mode == EyeTrackingMode.Manhatten:
            return distance.calculateDistanceManhatten(referencePoint, self.get_current_vector())
        elif mode == EyeTrackingMode.Maximum:
            return distance.calculateDistanceMaximum(referencePoint, self.get_current_vector())

    def calculate_pr(self):
        session = g.current_user.get_active_session()
        session.add_to_pr(self.prValue)

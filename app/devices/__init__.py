from flask import g
from app.models import Session


deviceClasses = {}
deviceManager = {}

def register_device_class(cls):
    deviceClasses[cls.__name__] = cls
    deviceManager[cls.__name__] = cls.get_device_manager()()
    return cls

from app.devices.eye_positional_tracking_device import EyePositionalTracker
from app.devices.eye_bounds_tracking_device import EyeBoundsTracker
from app.devices.eye_blinking_device import EyeBlinkTracker
from app.devices.head_movement_device import HeadMovementTracker
from app.devices.blood_pressure_device import BloodPressureTracker
from app.devices.breath_frequency_device import BreathFrequencyTracker
from app.devices.heart_frequenzy_device import HeartFrequencyTracker
from app.devices.skin_salvation_device import SkinSalvationTracker
from app.devices.tachygastric_device import TachygastricTracker
from app.devices.temperature_device import TemperatureTracker


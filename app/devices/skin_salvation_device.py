from app.devices import *
from app.devices.base_device import BaseDevice, BaseDeviceManager
from app import db


class SkinSalvationTrackerManager(BaseDeviceManager):

    def start_calibrating(self):
        if self.ConstantCompare:
            return
        self.add_to_compare(self.data.value)

    def start_tracking(self):
        if self.CompareValue == 0:
            print("WARNING: Reference value is 0. Set to 0.0001 to ensure comparison. Resulting Pr may be too sensitive. Adjust via manipulating the calibration value or by sending more calibration data.")
            self.CompareValue = 0.0001
        self.data.prValue = self.determine_pr_value() * self.weight

    def add_to_compare(self, value: float):
        self.CalibrationValue = self.CalibrationValue + value
        self.AmountCalibrationValues = self.AmountCalibrationValues + 1
        self.CompareValue = self.CalibrationValue/self.AmountCalibrationValues

    def determine_percent_difference(self, value: float, ref: float):
        difference = value/ref 
        if difference > 1:
            return difference - 1
        elif difference < 1:
            return (1 - difference) * (-1)
        else:
            return 0

    def determine_pr_value(self):
        currentValue = self.data.value
        ps = self.determine_percent_difference(currentValue, self.CompareValue)
        differences = {}
        invert = 1
        if ps  < 0:
            invert = -1
        for element in self.PrValueTable.keys():
            differences[element] = (ps - float(element)) * invert
        sor = sorted(differences.items(), key=lambda x:x[1])
        pr = next(iter(sor))
        return float(self.PrValueTable[pr[0]]) 
    

@register_device_class
class SkinSalvationTracker(BaseDevice):
    value = db.Column(db.FLOAT, index=True)

    @staticmethod
    def get_device_manager():
        return SkinSalvationTrackerManager

    def to_dict(self):
        data = super().to_dict()
        data['value'] = self.value
        return data

    def calculate_pr(self):
        session = g.current_user.get_active_session()
        session.add_to_pr(self.prValue)
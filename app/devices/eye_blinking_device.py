from app.devices import *
from app.devices.base_device import BaseDevice, BaseDeviceManager
from app import db
from datetime import datetime, timedelta
from enum import Enum


class EyeState(Enum):
    Open = 0
    Closed = 1

class EyeBlinkTrackerManager(BaseDeviceManager):

    lastEyeState = EyeState.Open
    currentEyeState = EyeState.Open 
    countBlinks = 0
    lastBlinkTimeStamp = None
    currentTimeDelta = 0

    ##Works perfect
    def start_calibrating(self):
        if self.ConstantCompare:
            return
        #print("--Calibration--")
        #print("Raw Data: ", self.data.to_dict())
        self.lastEyeState = self.currentEyeState
        self.currentEyeState = self.data.get_eye_state()
        #print("Last Eye State: ", self.lastEyeState)
        #print("current Eye State: ", self.currentEyeState)
        if self.did_eye_state_change(self.lastEyeState, self.currentEyeState):
            #print("Detected Changed State.")
            if self.open_to_close():
                #print("Detected Change from open to close.")
                #change from open to close
                if self.lastBlinkTimeStamp is not None:
                    #print("Found last Timestamp.")
                    #self.AmountCalibrationValues = self.AmountCalibrationValues + 1
                    #print("Current Amount of calubration values: ", self.amountCalibrationValues)
                    timestampLast = self.lastBlinkTimeStamp
                    timestampCurrent = self.data.timestamp
                    #print("Timestamp Last: ", timestampLast)
                    #print("Timestamp Current: ", timestampCurrent)
                    self.currentTimeDelta = (timestampCurrent - timestampLast).total_seconds()
                    self.add_to_compare(self.currentTimeDelta)
                    #self.CalibrationValue = self.CalibrationValue + (timestampCurrent - timestampLast).total_seconds()
                    #print("Calculated Calibration Value: ", self.calibrationValue)
                self.lastBlinkTimeStamp = self.data.timestamp
            else:
                #change from close to open
                pass
        

    def start_tracking(self):
        if self.AmountCalibrationValues == 0:
            print("WARNING: No calibration took place!")
            self.calibrationValue = 5
            #print("-!- New Calibration Value: ", self.CalibrationValue)
        #print("--Tracking--")
        #print("Raw data: ", self.data.to_dict())
        self.lastEyeState = self.currentEyeState
        self.currentEyeState = self.data.get_eye_state()
        #print("Last Eye State: ", self.lastEyeState)
        #print("current Eye State: ", self.currentEyeState)
        if self.did_eye_state_change(self.lastEyeState, self.currentEyeState):
            #print("Detected Changed State.")
            if self.open_to_close():
                #print("Detected Change from open to close.")
                if self.lastBlinkTimeStamp is not None:
                    #print("Found last Timestamp.")
                    timestampLast = self.lastBlinkTimeStamp
                    timestampCurrent = self.data.timestamp
                    #print("Timestamp Last: ", timestampLast)
                    #print("Timestamp Current: ", timestampCurrent)
                    self.currentTimeDelta = (timestampCurrent - timestampLast).total_seconds()
                    #print("Calculated Delta Value: ", self.calibrationValue)
                    self.data.prValue = self.determine_pr_value() * self.weight
                    #print("Berechneter Wahrscheinlichkeitswert: ", self.data.prValue)
                self.lastBlinkTimeStamp = self.data.timestamp

    def did_eye_state_change(self, state1: EyeState, state2: EyeState):
        return state1 != state2 

    def open_to_close(self):
        last = self.lastEyeState
        current = self.currentEyeState
        if last is EyeState.Open and current is EyeState.Closed:
            return True
        elif last is EyeState.Closed and current is EyeState.Open:
            return False
        else:
            return False

    def add_to_compare(self, value: float):
        self.CalibrationValue = self.CalibrationValue + value
        self.AmountCalibrationValues = self.AmountCalibrationValues + 1
        self.CompareValue = self.CalibrationValue / self.AmountCalibrationValues

    def determine_percent_difference(self, value: float, ref: float):
        difference = value/ref 
        if difference > 1:
            return difference - 1
        elif difference < 1:
            return (1 - difference) * (-1)
        else:
            return 0

    def add_to_compare(self, value: float):
        self.CalibrationValue = self.CalibrationValue + value
        self.AmountCalibrationValues = self.AmountCalibrationValues + 1
        self.CompareValue = self.CalibrationValue / self.AmountCalibrationValues

    def determine_pr_value(self):
        currentValue = self.currentTimeDelta
        ps = self.determine_percent_difference(currentValue, self.CompareValue)
        differences = {}
        invert = 1
        if ps  < 0:
            invert = -1
        for element in self.PrValueTable.keys():
            differences[element] = (ps - float(element)) * invert
        sor = sorted(differences.items(), key=lambda x:x[1])
        pr = next(iter(sor))
        return float(self.PrValueTable[pr[0]]) 

@register_device_class
class EyeBlinkTracker(BaseDevice):
    left = db.Column(db.BOOLEAN, index=True)
    right = db.Column(db.BOOLEAN, index=True)

    @staticmethod
    def get_device_manager():
        return EyeBlinkTrackerManager

    def to_dict(self):
        data = super().to_dict()
        data['left'] = self.left
        data['right'] = self.right
        return data
    
    def get_blink(self):
        return [self.left, self.right]

    def calculate_pr(self):
        session = g.current_user.get_active_session()
        session.add_to_pr(self.prValue)
                
    def get_eye_state(self):
        recentBlink = self.get_blink()
        if recentBlink[0] is False and recentBlink[1]:
            return EyeState.Open
        elif recentBlink[0] is True and recentBlink[1]:
            return EyeState.Closed
        else:
            return EyeState.Open
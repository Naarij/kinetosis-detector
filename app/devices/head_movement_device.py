from app.devices import *
from app.devices.base_device import BaseDevice, BaseDeviceManager
from app import db

class HeadMovementTrackerManager(BaseDeviceManager):

    def start_calibrating(self):
        if self.ConstantCompare:
            return
        self.add_to_compare((self.data.get_delta("yaw") + self.data.get_delta("pitch"))/2)

    def start_tracking(self):
        if self.CompareValue == 0:
            print("WARNING: Reference value is 0. Set to 0.0001 to ensure comparison. Resulting Pr may be too sensitive. Adjust via manipulating the calibration value or by sending more calibration data.")
            self.CompareValue = 0.0001
        self.data.prValue = self.determine_pr_value() * self.weight

    def add_to_compare(self, value: float):
        self.CalibrationValue = self.CalibrationValue + value
        self.AmountCalibrationValues = self.AmountCalibrationValues + 1
        self.CompareValue = self.CalibrationValue / self.AmountCalibrationValues

    def determine_percent_difference(self, value: float, ref: float):
        difference = value/ref 
        if difference > 1:
            return difference - 1
        elif difference < 1:
            return (1 - difference) * (-1)
        else:
            return 0

    def determine_pr_value(self):
        currentValue = (self.data.get_delta("yaw") + self.data.get_delta("pitch"))/2
        ps = self.determine_percent_difference(currentValue, self.CompareValue)
        differences = {}
        invert = 1
        if ps  < 0:
            invert = -1
        for element in self.PrValueTable.keys():
            differences[element] = (ps - float(element)) * invert
        sor = sorted(differences.items(), key=lambda x:x[1])
        pr = next(iter(sor))
        return float(self.PrValueTable[pr[0]]) 

@register_device_class
class HeadMovementTracker(BaseDevice):
    yaw1 = db.Column(db.FLOAT, index=True)
    pitch1 = db.Column(db.FLOAT, index=True)
    roll1 = db.Column(db.FLOAT, index=True)
    yaw2 = db.Column(db.FLOAT, index=True)
    pitch2 = db.Column(db.FLOAT, index=True)
    roll2 = db.Column(db.FLOAT, index=True)

    @staticmethod
    def get_device_manager():
        return HeadMovementTrackerManager

    def to_dict(self):
        data = super().to_dict()
        data['yaw1'] = self.yaw1
        data['pitch1'] = self.pitch1
        data['roll1'] = self.roll1
        data['yaw2'] = self.yaw2
        data['pitch2'] = self.pitch2
        data['roll2'] = self.roll2
        return data

    def get_delta(self):
        rotMax = 360
        deltaYaw1 = abs(rotMax - self.yaw2 + self.yaw1)
        deltaPitch1 = abs(rotMax - self.pitch2 + self.pitch1)
        deltaRoll1 = abs(rotMax - self.roll2 + self.roll1)
        deltaYaw2 = abs(self.yaw2 - self.yaw1)
        deltaPitch2 = abs(self.pitch2 - self.pitch1)
        deltaRoll2 = abs(self.roll2 - self.roll1)

        deltaYaw = min(deltaYaw1, deltaYaw2)
        deltaPitch = min(deltaPitch1, deltaPitch2)
        deltaRoll = min(deltaRoll1, deltaRoll2)

        return (deltaYaw, deltaPitch, deltaRoll)

    def get_delta(self, val: str):
        rotMax = 360
        data = self.to_dict()
        delta1 = abs(rotMax - data[val + "2"] + data[val + "1"])
        delta2 = abs(data[val + "2"] - data[val + "1"])

        return (min(delta1, delta2))

    def calculate_pr(self):
        session = g.current_user.get_active_session()
        session.add_to_pr(self.prValue)
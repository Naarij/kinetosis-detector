from datetime import datetime
from app import db
from sqlalchemy.ext.declarative import declared_attr


class BaseDeviceManager:
    __abstract__= True

    PrValueTable = { -0.2: -0.05, -0.4: -0.1, 0.2: 0.05, 0.4: 0.1 }
    CompareValue = 0
    CalibrationValue = 0
    AmountCalibrationValues = 0
    ConstantCompare = False

    data = None
    weight = 1

    def reset(self):
        AmountCalibrationValues = 0
        CalibrationValue = 0
        CompareValue = 0
        data = None

    def get_from_session(self, session):
        return

class BaseDevice(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    calibration = db.Column(db.BOOLEAN, index=True)

    prValue = 0

    @declared_attr
    def id_session(cls):
        return db.Column(db.Integer, db.ForeignKey('session.id'))

    def to_dict(self):
        data = {
            'id': self.id,
            'timestamp': self.timestamp,
            'id_session': self.id_session
        }
        return data

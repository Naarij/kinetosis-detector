from app.devices import *
from app.devices.base_device import BaseDevice, BaseDeviceManager
from app import db
from enum import Enum

class BreathState(Enum):
    Inhale = 0,
    Exhale = 1

class BreathFrequencyManager(BaseDeviceManager):

    lastBreathTimeStamp = None
    oldBreathState = BreathState.Exhale
    currentBreathState = BreathState.Exhale
    currentTimeDelta = 0

    def start_calibrating(self):
        if self.ConstantCompare:
            return
                #print("--Calibration--")
        #print("Raw Data: ", self.data.to_dict())
        self.oldBreathState = self.currentEyeState
        self.currentBreathState = self.data.get_breath_state()
        #print("Last Eye State: ", self.lastEyeState)
        #print("current Eye State: ", self.currentEyeState)
        if self.did_breath_state_change(self.oldBreathState, self.currentBreathState):
            #print("Detected Changed State.")
            if self.inhale_to_exhale():
                #print("Detected Change from open to close.")
                #change from open to close
                if self.lastBreathTimeStamp is not None:
                    #print("Found last Timestamp.")
                    #self.AmountCalibrationValues = self.AmountCalibrationValues + 1
                    #print("Current Amount of calubration values: ", self.amountCalibrationValues)
                    timestampLast = self.lastBreathTimeStamp
                    timestampCurrent = self.data.timestamp
                    #print("Timestamp Last: ", timestampLast)
                    #print("Timestamp Current: ", timestampCurrent)
                    self.currentTimeDelta = (timestampCurrent - timestampLast).total_seconds()
                    self.add_to_compare(self.currentTimeDelta)
                    #self.CalibrationValue = self.CalibrationValue + (timestampCurrent - timestampLast).total_seconds()
                    #print("Calculated Calibration Value: ", self.calibrationValue)
                self.lastBreathTimeStamp = self.data.timestamp
            else:
                #change from close to open
                pass

    def start_tracking(self):
        if self.CompareValue == 0:
            print("WARNING: Reference value is 0. Set to 0.0001 to ensure comparison. Resulting Pr may be too sensitive. Adjust via manipulating the calibration value or by sending more calibration data.")
            self.CompareValue = 0.0001
        self.oldBreathState = self.currentEyeState
        self.currentBreathState = self.data.get_breath_state()
        #print("Last Eye State: ", self.lastEyeState)
        #print("current Eye State: ", self.currentEyeState)
        if self.did_breath_state_change(self.oldBreathState, self.currentBreathState):
            #print("Detected Changed State.")
            if self.inhale_to_exhale():
                #print("Detected Change from open to close.")
                #change from open to close
                if self.lastBreathTimeStamp is not None:
                    #print("Found last Timestamp.")
                    #self.AmountCalibrationValues = self.AmountCalibrationValues + 1
                    #print("Current Amount of calubration values: ", self.amountCalibrationValues)
                    timestampLast = self.lastBreathTimeStamp
                    timestampCurrent = self.data.timestamp
                    #print("Timestamp Last: ", timestampLast)
                    #print("Timestamp Current: ", timestampCurrent)
                    self.currentTimeDelta = (timestampCurrent - timestampLast).total_seconds()
                    self.data.prValue = self.determine_pr_value() * self.weight
                    #self.CalibrationValue = self.CalibrationValue + (timestampCurrent - timestampLast).total_seconds()
                    #print("Calculated Calibration Value: ", self.calibrationValue)
                self.lastBreathTimeStamp = self.data.timestamp
            else:
                #change from close to open
                pass

    def add_to_compare(self, value: float):
        self.CalibrationValue = self.CalibrationValue + value
        self.AmountCalibrationValues = self.AmountCalibrationValues + 1
        self.CompareValue = self.CalibrationValue/self.AmountCalibrationValues

    def determine_percent_difference(self, value: float, ref: float):
        difference = value/ref 
        if difference > 1:
            return difference - 1
        elif difference < 1:
            return (1 - difference) * (-1)
        else:
            return 0

    def did_breath_state_change(self, state1: BreathState, state2: BreathState):
        return state1 != state2

    def inhale_to_exhale(self):
        last = self.oldBreathState
        current = self.currentBreathState
        if last is BreathState.Inhale and current is BreathState.Exhale:
            return True
        else:
            return False

    def determine_pr_value(self):
        currentValue = self.currentTimeDelta
        ps = self.determine_percent_difference(currentValue, self.CompareValue)
        differences = {}
        invert = 1
        if ps  < 0:
            invert = -1
        for element in self.PrValueTable.keys():
            differences[element] = (ps - float(element)) * invert
        sor = sorted(differences.items(), key=lambda x:x[1])
        pr = next(iter(sor))
        return float(self.PrValueTable[pr[0]]) 

@register_device_class
class BreathFrequencyTracker(BaseDevice):
    value = db.Column(db.BOOLEAN, index=True)

    @staticmethod
    def get_device_manager():
        return BreathFrequencyManager

    def to_dict(self):
        data = super().to_dict()
        data['value'] = self.value
        return data

    def get_breath_state(self):
        if self.value:
            return BreathState.Inhale
        return BreathState.Exhale

    def calculate_pr(self):
        session = g.current_user.get_active_session()
        session.add_to_pr(self.prValue)
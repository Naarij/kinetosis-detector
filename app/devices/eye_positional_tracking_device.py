from app.devices import *
from app.devices.base_device import BaseDevice, BaseDeviceManager
from app import db
from enum import IntEnum
from sqlalchemy import func
from app.util import distance
import queue
import math

class EyeTrackingMode(IntEnum):
    Euclid = 0
    Manhatten = 1
    Maximum = 2

class EyePositionalTrackingManager(BaseDeviceManager):

    Mode = EyeTrackingMode.Euclid

    def start_calibrating(self):
        if self.ConstantCompare:
            return
        self.add_to_compare(self.data.get_current_distance(self.Mode))

    def start_tracking(self):
        if self.CompareValue == 0:
            print("WARNING: Reference value is 0. Set to 0.0001 to ensure comparison. Resulting Pr may be too sensitive. Adjust via manipulating the calibration value or by sending more calibration data.")
            self.CompareValue = 0.0001
        self.data.prValue = self.determine_pr_value() * self.weight

    def add_to_compare(self, value: float):
        self.CalibrationValue = self.CalibrationValue + value
        self.AmountCalibrationValues = self.AmountCalibrationValues + 1
        self.CompareValue = self.CalibrationValue / self.AmountCalibrationValues

    def determine_percent_difference(self, value: float, ref: float):
        difference = value/ref 
        if difference > 1:
            return difference - 1
        elif difference < 1:
            return (1 - difference) * (-1)
        else:
            return 0

    def determine_percent_difference(self, value: float, ref: float):
        difference = value/ref 
        if difference > 1:
            return difference - 1
        elif difference < 1:
            return (1 - difference) * (-1)
        else:
            return 0

    def determine_pr_value(self):
        currentValue = self.data.get_current_distance(self.Mode)
        ps = self.determine_percent_difference(currentValue, self.CompareValue)
        differences = {}
        invert = 1
        if ps  < 0:
            invert = -1
        for element in self.PrValueTable.keys():
            differences[element] = (ps - float(element)) * invert
        sor = sorted(differences.items(), key=lambda x:x[1])
        pr = next(iter(sor))
        return float(self.PrValueTable[pr[0]])          


@register_device_class
class EyePositionalTracker(BaseDevice):
    x1 = db.Column(db.FLOAT, index=True)
    y1 = db.Column(db.FLOAT, index=True)
    z1 = db.Column(db.FLOAT, index=True)
    x2 = db.Column(db.FLOAT, index=True)
    y2 = db.Column(db.FLOAT, index=True)
    z2 = db.Column(db.FLOAT, index=True)

    prValue = 0

    def to_dict(self):
        data = super().to_dict()
        data['x1'] = self.x1
        data['y1'] = self.y1
        data['z1'] = self.z1
        data['x2'] = self.x2
        data['y2'] = self.y2
        data['z2'] = self.z2
        data['calibration'] = self.calibration
        return data

    @staticmethod
    def get_device_manager():
        return EyePositionalTrackingManager

    def get_last_vector(self):
        return [self.x1, self.y1, self.z1]

    def get_current_vector(self):
        return [self.x2, self.y2, self.z2]

    def get_current_distance(self, mode):
        if mode == EyeTrackingMode.Euclid:
            return distance.calculateDistanceEuclid(self.get_last_vector(), self.get_current_vector())
        elif mode == EyeTrackingMode.Manhatten:
            return distance.calculateDistanceManhatten(self.get_last_vector(), self.get_current_vector())
        elif mode == EyeTrackingMode.Maximum:
            return distance.calculateDistanceMaximum(self.get_last_vector(), self.get_current_vector())

    def calculate_pr(self):
        session = g.current_user.get_active_session()
        session.add_to_pr(self.prValue)

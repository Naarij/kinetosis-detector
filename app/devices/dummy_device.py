from app.devices import *
from app.devices.base_device import BaseDevice, BaseDeviceManager
from app import db


class DummyManager(BaseDeviceManager):
    a = 0

@register_device_class
class DummyDevice(BaseDevice):
    value = db.Column(db.Float, index=True)

    def to_dict(self):
        data = super().to_dict()
        data['value'] = self.value
        return data

    def calculate_pr(self):
        session = g.current_user.get_active_session()
        session.add_to_pr(self.value)

    @staticmethod
    def get_device_manager():
        return DummyManager
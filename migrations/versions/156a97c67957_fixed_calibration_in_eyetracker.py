"""fixed calibration in eyetracker

Revision ID: 156a97c67957
Revises: 1c63977fa1a2
Create Date: 2020-08-17 19:26:41.354700

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '156a97c67957'
down_revision = '1c63977fa1a2'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    #op.execute('ALTER TABLE eye_blink_tracker ALTER COLUMN calibration TYPE boolean USING calibration::boolean')
    with op.batch_alter_table('eye_blink_tracker', schema=None) as batch_op:
        batch_op.alter_column('calibration',
               existing_type=postgresql.DOUBLE_PRECISION(precision=53),
               type_=sa.BOOLEAN(),
               existing_nullable=True,
               postgresql_using='calibration::int::boolean')
    #op.execute('ALTER TABLE eye_positional_tracker ALTER COLUMN calibration TYPE boolean USING calibration::boolean')
    with op.batch_alter_table('eye_positional_tracker', schema=None) as batch_op:
        batch_op.alter_column('calibration',
               existing_type=postgresql.DOUBLE_PRECISION(precision=53),
               type_=sa.BOOLEAN(),
               existing_nullable=True,
               postgresql_using='calibration::int::boolean')
    #op.execute('ALTER TABLE head_movement_tracker ALTER COLUMN calibration TYPE boolean USING calibration::boolean')
    with op.batch_alter_table('head_movement_tracker', schema=None) as batch_op:
        batch_op.alter_column('calibration',
               existing_type=postgresql.DOUBLE_PRECISION(precision=53),
               type_=sa.BOOLEAN(),
               existing_nullable=True,
               postgresql_using='calibration::int::boolean')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.execute('ALTER TABLE head_movement_tracker ALTER COLUMN calibration TYPE double_precision USING calibration::double_precision')
    with op.batch_alter_table('head_movement_tracker', schema=None) as batch_op:
        batch_op.alter_column('calibration',
               existing_type=sa.BOOLEAN(),
               type_=postgresql.DOUBLE_PRECISION(precision=53),
               existing_nullable=True,
               postgresql_using='calibration::int::double_precision')
    op.execute('ALTER TABLE eye_positional_tracker ALTER COLUMN calibration TYPE double_precision USING calibration::double_precision')
    with op.batch_alter_table('eye_positional_tracker', schema=None) as batch_op:
        batch_op.alter_column('calibration',
               existing_type=sa.BOOLEAN(),
               type_=postgresql.DOUBLE_PRECISION(precision=53),
               existing_nullable=True,
               postgresql_using='calibration::int::double_precision')
    op.execute('ALTER TABLE eye_blink_tracker ALTER COLUMN calibration TYPE double_precision USING calibration::double_precision')
    with op.batch_alter_table('eye_blink_tracker', schema=None) as batch_op:
        batch_op.alter_column('calibration',
               existing_type=sa.BOOLEAN(),
               type_=postgresql.DOUBLE_PRECISION(precision=53),
               existing_nullable=True,
               postgresql_using='calibration::int::double_precision')

    # ### end Alembic commands ###

import os

basedir = os.path.abspath(os.path.dirname(__file__))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'SehrSehrGeheimerSchluesselDerUnbedingtGeaendertWerdenSollte'

    SQLALCHEMY_DATABASE_URI = "postgresql://flask_sensor_server:geheim@localhost/flask_sensor_server_db"
    #os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
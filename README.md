# Kinetosis Detector

## What is this about?

The "Kinetosis Detector" is a flask webserver, which was made to calculate a probability of occurence for kinetosis. It registers different sensor data via its routes and calclates a vlaue representing this probability. If this value exceeds a user-specified threshold, adaption strings, which can be requested, can be used. Communication of the sensors take place via the HTTP protocol. This server works with an Adaption Asset in Unity located at https://gitlab.com/Naarij/kinetosisadaptionpackageunity. <br>
The original server was deisgned and implemented by Samuel Blickle (https://gitlab.com/SamuelBlickle) in his repository https://gitlab.com/SamuelBlickle/kinetosis-detector. This repository is forked from it. <br>
This project was developed as a bachelor thesis at the University of Applied Sciences Aalen and will be maintained by more contributors throughout different projects.

## Requires

To install and use the server, there are some remarks and prerequsities:

- Python 3.7 or above
- Works best on Linux (tested for Mint 17 and Ubuntu 20.01), Windows is possible but has some hiccups
- PostGreSQL 12 or above
- Everything in [Requirements](requirements)

**To access ther server from another computer a network connection is required. If no network connection is possible, please consider to run it localhost.**

## Install

- Make sure, Python 3.7 or later is running on your machine.
- Clone the repository to your preferred location.
- Download PostGreSQL and install it.
- After installation run the psql shell, authenticate yourself and type the following commands:
```bash
CREATE DATABASE flask_sensor_server_db;
CREATE USER flask_sensor_server WITH ENCRYPTED PASSWORD 'geheim';
GRANT ALL PRIVILEGES ON DATABASE flask_sensor_server_db TO flask_sensor_server;
```
- You should now have created a database called "flask_sensor_server_db", which you can see via the command \l and a user called "flask_sensor_server" with the password "geheim" with permission to the database, which you can see via the command \du. Those informations should match with the data in [config.py](config.py).
- Now head to the webserver and install all requirements (most IDEs recognize the requirements and ask you to do it). Preferably open up a new virutal environment for this project.
- After installation run the command `flask db upgrade` to create the database and its tables.
- You can now run the webserver with the command `flask run --host="<ipaddress>"`.
